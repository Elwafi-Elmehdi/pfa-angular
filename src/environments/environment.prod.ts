/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  firebase :{
    apiKey: "AIzaSyCNpz_q7Q_2UeV434wkZQ_F5_ZHVuj_UYs",
    authDomain: "safely-systems.firebaseapp.com",
    projectId: "safely-systems",
    storageBucket: "safely-systems.appspot.com",
    messagingSenderId: "242820385043",
    appId: "1:242820385043:web:1911ab0c8835617a66b2e6",
    measurementId: "G-VT36REZM9K"
    vapidKey: "BJpXZOBvs9cZnl_eJ-VXTsVy79E9vnK-XS9RmhRfXH9WdMHi0HqaadwyajI0mXWyjkxtGnZl3UX86taFZuBisfw"
  },
  url: '/api/api',
};
