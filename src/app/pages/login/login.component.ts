import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import {NbToastrService} from "@nebular/theme";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService,
              private toastService:NbToastrService,
              private router:Router) {
    this.user = new User();
  }

  user: User = null;

  ngOnInit(): void {
    if(localStorage.getItem("access_token") != null && localStorage.getItem("access_token") !== '')
      this.router.navigate(['pages/iot-dashboard']);
    else {
      localStorage.clear();
    }
  }

  login(user: User) {
    if (user.email != null && user.password != null){
      this.authService.login(user);
    }else {
      this.toastService.warning("Please enter a valid email/password","Notification Center")
    }
  }
}
