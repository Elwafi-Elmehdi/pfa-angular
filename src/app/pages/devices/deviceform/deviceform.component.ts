import { Component, OnInit } from '@angular/core';
import {NbDialogRef, NbToastrService} from '@nebular/theme';
import { Device } from '../../models/device';
import {DeviceService} from "../../services/device.service";

@Component({
  selector: 'ngx-deviceform',
  templateUrl: './deviceform.component.html',
  styleUrls: ['./deviceform.component.scss']
})
export class DeviceformComponent implements OnInit {

  constructor(protected dialogService: NbDialogRef<any>,
              private service: DeviceService,
              private toastService: NbToastrService) {
    this.device = new Device();
  }

  public device: Device;
  ngOnInit(): void {
  }

  close() {
    this.dialogService.close();
  }

  save() {
    if (this.device.type !=null && this.device.name !=null && this.device.reference !=null ){
      this.service.saveDevice(this.device).subscribe((res:Device) => {
        this.dialogService.close();
        this.toastService.success('New Device Added!',"Notification Hub");
      });
    }
    else {
      this.toastService.warning("Please enter a valid device informartions.","Notification Center")
    }
  }

}
