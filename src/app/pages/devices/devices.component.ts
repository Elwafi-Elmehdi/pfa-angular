import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import { Device } from '../models/device';
import { DeviceService } from '../services/device.service';
import { DeviceformComponent } from './deviceform/deviceform.component';

@Component({
  selector: 'ngx-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {

  constructor(private service: DeviceService,
              private dialogService: NbDialogService,
              private toastService:NbToastrService) {

  }

  ngOnInit(): void {
    this.service.loadAll().subscribe(res => {
      this.devices = res;
    });
  }
  public delete(id: number|undefined) {
      this.service.deleteDeviceById(id).subscribe(res => {
        console.log(res);
      });
  }

  viewDevice() {
  }

  viewDeviceForm() {
    this.dialogService.open(DeviceformComponent, {
      autoFocus: true,
      context: 'test',
      closeOnEsc: true,
    });
  }
  get devices(): Device[] {
    return this.service.devices;
  }
  set devices(devices: Device[]) {
    this.service.devices = devices;
  }

  activate(device: Device) {
    device.status = !device.status
    this.service.updateDeviceById(device.id, device).subscribe(res => {
      this.toastService.primary(`Device is ${device.status?'Activated':'Desactiveted'}`);
    });
  }
}
