import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Device } from '../models/device';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private http: HttpClient) { }

  private readonly url: string = `${environment.url}/devices`;
  private _device: Device | null;
  private _devices: Device[] | null;

  public loadAll() {
    return this.http.get<Device[]>(this.url + '/');
  }
  public getDeviceById(id: number) {
    return this.http.get<Device>(this.url + `/${id}/`);
  }
  public saveDevice(device: Device) {
      return this.http.post(this.url+'/', device);
  }
  public deleteDeviceById(id: number) {
      return this.http.delete(this.url + `/${id}`);
  }
  public updateDeviceById(id: number, device: any) {
      return this.http.patch(this.url + `/${id}`, device);
  }
  get device(): Device | null {
    return this._device;
  }

  set device(value: Device | null) {
    this._device = value;
  }

  get devices(): Device[] | null {
    return this._devices;
  }

  set devices(value: Device[] | null) {
    this._devices = value;
  }
}
