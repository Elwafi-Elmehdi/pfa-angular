import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { LoginResult } from '../models/login-result';
import { User } from '../models/user';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private router: Router) {
    this.user = new User();
  }
  private readonly url: string = `${environment.url}/`;
  private user: User;
  private access_token: string | null = null;
  private refresh_token: string | null = null;

  public login(user: User) {
    this.http.post<LoginResult>(this.url + '/token/', user).subscribe(res => {
      this.setLocalStorage(res);
      this.router.navigate(['pages/iot-dashboard']);
    });
  }

  public logout() {
    this.clearLocalStorage();
    this.router.navigate(['login']);
  }

  public setLocalStorage(res: LoginResult) {
    this.storageService.set('access_token', res.access);
    this.storageService.set('refresh_token', res.refresh);
  }

  public clearLocalStorage() {
    this.storageService.remove('access_token');
    this.storageService.remove('refresh_token');
  }

  private getTokenRemainingTime() {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      return 0;
    }
    const jwtToken = JSON.parse(atob(accessToken.split('.')[1]));
    const expires = new Date(jwtToken.exp * 1000);
    return expires.getTime() - Date.now();
  }

  public isLoggedIn(): boolean {
    if(localStorage.getItem("access_token") != null && localStorage.getItem("access_token") !== ''){
        if( this.getTokenRemainingTime() > 0){
          return true;
        }
      } else {
      this.logout();
      return false;
    }
    return false;
  }

}
