export class LoginResult {
    access: string | null;
    refresh: string | null;
}
