export class Device {
    id: number | undefined;
    created_at: Date |undefined;
    status:boolean |undefined;
    owner: number | undefined;
    name: string | undefined;
    type: string | undefined;
    reference: string | undefined;
    data: string | undefined;
    description: string | undefined;
}
