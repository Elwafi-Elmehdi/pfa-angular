import { NgModule } from '@angular/core';
import {
    NbAccordionModule,
    NbButton,
    NbButtonModule,
    NbCardModule,
    NbFormFieldModule,
    NbIconModule,
    NbInputModule,
    NbMenuModule,
    NbSelectComponent,
    NbSelectModule,
    NbTagModule
} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { DevicesComponent } from './devices/devices.component';
import { DeviceformComponent } from './devices/deviceform/deviceform.component';
import { FormsModule } from '@angular/forms';
import {LoginComponent} from "./login/login.component";
import {GuardsGuard} from "./controller/guards.guard";

@NgModule({
    imports: [
        PagesRoutingModule,
        FormsModule,
        ThemeModule,
        NbMenuModule,
        DashboardModule,
        ECommerceModule,
        MiscellaneousModule,
        NbCardModule,
        NbInputModule,
        NbButtonModule,
        NbIconModule,
        NbTagModule,
        NbFormFieldModule,
        NbSelectModule,
        NbAccordionModule,
    ],
  declarations: [
    PagesComponent,
    DevicesComponent,
    LoginComponent,
    DeviceformComponent,
  ],
  providers:[
    GuardsGuard,
  ]
})
export class PagesModule {
}
